import {Album} from '../../models/album'

export const albumsFake: Album[] = [
  {
    artist: 'Metallica',
    genres: ['Heavy Metal', 'Thrash Metal'],
    id: '1',
    title: 'Master of Puppets',
    imageUrl:
      'https://universalmusic.vteximg.com.br/arquivos/ids/162161-1000-1000/cd-metallica-master-of-puppets-cd-metallica-master-of-puppets-00042283814127-268381412.jpg?v=637308823686730000'
  },
  {
    artist: 'Metallica',
    genres: ['Heavy Metal', 'Thrash Metal'],
    id: '2',
    title: '...And Justice for All',
    imageUrl:
      'https://whiplash.net/imagens_promo/metallica_capa_andjusticeforall.jpg'
  },
  {
    artist: 'Metallica',
    genres: ['Heavy Metal', 'Thrash Metal'],
    id: '3',
    title: 'Hardwired... to Self-Destruct',
    imageUrl:
      'https://upload.wikimedia.org/wikipedia/pt/3/3b/Metallica_-_Hardwired%E2%80%A6To_Self-Destruct_-_2016.jpg'
  }
]
