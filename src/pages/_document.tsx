import React from 'react'
import Document, { Html, Head, Main, NextScript } from 'next/document'

export default class MyDocument extends Document {
  render() {
    return (
      <Html>
        <Head>
          <meta
            name={'description'}
            content={
              'Your portal to follow the daily releases of the Heavy Metal universe!'
            }
          />
          <meta
            name="keywords"
            content="Release, Albums, Full-Lenght, CD, Alternative metal, Black metal, Death metal, Doom metal, Folk Metal, Industrial Metal, Heavy Metal, Post Metal, Power Metal, Progressive Metal, Speed Metal, Thrash Metal, Symphonic metal"
          />
          <meta
            name="google-site-verification"
            content="N7-A2y5eF50F_vFu5m6-z2K6HdCfnYNp5p-9YYwxacc"
          />
          <script
            data-ad-client="ca-pub-2035106109594006"
            async
            src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"
          />
          <script
            dangerouslySetInnerHTML={{
              __html: `
               (adsbygoogle = window.adsbygoogle || []).push({
                   google_ad_client: "ca-pub-2035106109594006",
                   enable_page_level_ads: true
              });
                `
            }}
          />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}
