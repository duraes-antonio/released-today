import React, { useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import Modal from 'react-modal'
import ModalAlbum from '../../components/modal-album/modal-album'
import { MAAlbum } from '../../services/metal-archives.service'
import { Album } from '../../models/album'

Modal.setAppElement('#__next')

const AlbumPage = (props: { albumId: number }): JSX.Element => {
  const router = useRouter()
  const [showModal, setShowModal] = useState(true)
  useEffect(() => {
    router.prefetch('/')
  }, [])

  const updateQuery = (dateRelease: Date) => {
    router.push(
      {
        pathname: '/',
        query: { date: encodeURI(dateRelease.toISOString().split('T')[0]) }
      },
      undefined,
      { shallow: true }
    )
  }

  const _setShowModal = (show: boolean) => {
    setShowModal(show)
    if (!show) {
      updateQuery(new Date())
    }
  }
  const selectedAlbum: MAAlbum = {
    date: new Date(),
    id: props.albumId,
    name: 'Teste',
    artist: 'Teste',
    format: 'Teste',
    imageUrl: 'https://www.metal-archives.com/images/8/8/6/0/886040.jpg?4237',
    urlAlbum: 'https://www.metal-archives.com/images/8/8/6/0/886040.jpg?4237'
  }

  return (
    <>
      {/* <Modal */}
      {/*  isOpen={true} */}
      {/*  onRequestClose={() => router.push('/')} */}
      {/*  contentLabel="Post modal" */}
      {/* > */}
      {/* </Modal> */}
      <ModalAlbum
        album={selectedAlbum}
        setShowModal={_setShowModal}
        showModal={showModal}
        // onRequestClose={() => router.push('/')}
      />
    </>
  )
}

export default AlbumPage

export function getStaticProps({ params: { albumId } }) {
  return { props: { albumId: albumId } }
}

export function getStaticPaths() {
  return {
    paths: [886040, 882885].map(albumId => ({
      params: { albumId: albumId.toString() }
    })),
    fallback: true
  }
}
