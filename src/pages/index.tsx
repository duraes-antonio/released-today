import Head from 'next/head'
import styles from '../styles/Home.module.css'
import React, { useState } from 'react'
import Footer from '../components/footer/footer'
import Banner from '../components/banner/banner'
import DateBar from '../components/date/date'
import CardList from '../components/card-list/card-list'
import { useRouter } from 'next/router'
import { ModalAlbum } from '../components/modal-album/modal-album'
import { MAAlbum } from '../services/metal-archives.service'

const Home = (props: { date?: string }): JSX.Element => {
  const router = useRouter()
  const dateFromURL =
    props.date && new Date(Date.parse(props.date.replace('-', '/')))
  const [initialDate, setDate] = useState(dateFromURL ?? new Date())
  const [showModal, setShowModal] = useState(false)
  const [selectedAlbum, setAlbum] = useState(null)
  const updateQuery = dateRelease => {
    router.push(
      {
        pathname: '',
        query: { date: encodeURI(dateRelease) }
      },
      undefined,
      { shallow: true }
    )
  }

  const setDateAndUpdateUrl = (dateRelease: Date) => {
    setDate(dateRelease)
    updateQuery(dateRelease.toISOString().split('T')[0])
  }

  const openModal = (a: MAAlbum) => {
    setAlbum(a)
    setShowModal(prev => !prev)
  }

  const _setShowModal = (show: boolean) => {
    if (!show) {
      setDateAndUpdateUrl(initialDate)
    }
    setShowModal(show)
  }

  return (
    <div className={styles.container}>
      <Head>
        <title>Released Today - Heavy Metal</title>
        <link rel="icon" href="/favicon.ico" />
        <meta
          httpEquiv={'Content-Type'}
          content={'text/html'}
          charSet={'utf-8'}
        />
        <meta
          name={'description'}
          content={
            'Your portal to follow the daily releases of the Heavy Metal universe!'
          }
        />
        <meta
          name="keywords"
          content="Release, Albums, Full-Lenght, CD, Alternative metal, Black metal, Death metal, Doom metal, Folk Metal, Industrial Metal, Heavy Metal, Post Metal, Power Metal, Progressive Metal, Speed Metal, Thrash Metal, Symphonic metal"
        />
        <meta
          name={'viewport'}
          content={'width=device-width, initial-scale=1'}
        />
        <meta
          name="google-site-verification"
          content="N7-A2y5eF50F_vFu5m6-z2K6HdCfnYNp5p-9YYwxacc"
        />
      </Head>
      <ModalAlbum
        album={selectedAlbum}
        setShowModal={_setShowModal}
        showModal={showModal}
        // onRequestClose={() => router.push('/')}
      />
      <main className={styles.main}>
        <Banner date={initialDate} />
        <DateBar initial={initialDate} cb={setDateAndUpdateUrl} />
        <CardList date={initialDate} selectCardCallback={a => openModal(a)} />
      </main>
      <Footer />
    </div>
  )
}

Home.getInitialProps = async ctx => {
  const date = ctx.query.date
  return { date }
}

export default Home
