import useSWR from 'swr'

export const useFetch = <T>(url: string): { data: T; error } => {
  const { data, error } = useSWR(url, async url => {
    const res = await fetch(url)
    return await res.json()
  })
  return { data, error }
}

export interface FilterAlbums {
  startDate: Date
  endDate?: Date
}

export interface MAAlbum {
  artist: string
  date: Date | null
  id: number
  imageUrl?: string
  format: string
  name: string
  urlAlbum: string
}
