export interface Album {
  artist: string
  id: string
  title: string
  genres: string[]
  imageUrl?: string
}
