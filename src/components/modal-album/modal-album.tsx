import React, { SetStateAction, useCallback, useEffect, useRef } from 'react'
import styles from './modal-album.module.scss'
import { animated, useSpring } from 'react-spring'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons/faTimes'
import { MAAlbum } from '../../services/metal-archives.service'

export const ModalAlbum = (props: {
  showModal: boolean
  setShowModal: (v: SetStateAction<boolean>) => void
  album: MAAlbum
}): JSX.Element => {
  console.log('ALBUM', props.album)
  const modalRef = useRef()
  const animation: any = useSpring({
    config: { duration: 200 },
    opacity: props.showModal ? 1 : 0,
    transform: props.showModal ? 'translateY(0%)' : 'translateY(-100%)'
  })

  const closeModal = e => {
    if (modalRef.current === e.target) {
      props.setShowModal(false)
    }
  }

  const keyPress = useCallback(
    e => {
      if (e.key === 'Escape' && props.showModal) {
        props.setShowModal(false)
      }
    },
    [props.setShowModal, props.showModal]
  )

  useEffect(() => {
    document.addEventListener('keydown', keyPress)
    return () => document.removeEventListener('keydown', keyPress)
  }, [keyPress])

  if (!props.showModal) {
    return null
  }

  return (
    <>
      <div className={styles.Background} onClick={closeModal} ref={modalRef}>
        <animated.div style={animation}>
          <div className={styles.ModalWrapper}>
            <img
              className={styles.ModalImg}
              src={props.album.imageUrl}
              alt={props.album.name + ' cover art'}
            />
            <div className={styles.ModalContent}>
              <h1>{props.album.name}</h1>
              <h2>{props.album.artist}</h2>
              <p>Get exclusive access to our next launch.</p>
              <button>Join Now</button>
            </div>
            <a
              aria-label="Close modal"
              className={styles.CloseModalButton}
              onClick={() => props.setShowModal(false)}
            >
              <FontAwesomeIcon color="black" icon={faTimes} size={'2x'} />
            </a>
          </div>
        </animated.div>
      </div>
    </>
  )
}

export default ModalAlbum
