import React from 'react'
import styles from './footer.module.scss'

const Footer = (): JSX.Element => {
  return (
    <footer className={styles.footer}>
      <span>
        All rights reversed. Powered by{' '}
        <a
          href="https://github.com/duraes-antonio"
          target="_blank"
          rel="noopener noreferrer"
        >
          @gseis
        </a>
      </span>
    </footer>
  )
}

export default Footer
