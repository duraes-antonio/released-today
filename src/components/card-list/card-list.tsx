import styles from './card-list.module.scss'
import CardAlbum from '../card-album/card'
import React, { useMemo } from 'react'
import {
  FilterAlbums,
  MAAlbum,
  useFetch
} from '../../services/metal-archives.service'
import { routes } from '../../consts/routes'
import Link from 'next/link'

const buildUrl = (f: FilterAlbums): string => {
  console.log('BUILD URL')
  const dateStart = f.startDate?.toISOString().split('T')[0] ?? ''
  const dateEnd = f.endDate?.toISOString().split('T')[0] ?? ''
  return `${routes.urlBackend}/metal-archives?startDate=${dateStart}&endDate=${dateEnd}`
}

const CardList = (props: {
  date: Date
  selectCardCallback?: (a: MAAlbum) => void
}): JSX.Element => {
  const requestUrl = useMemo(() => buildUrl({ startDate: props.date }), [
    props.date
  ])
  const { data } = useFetch<MAAlbum[]>(requestUrl)

  if (!data) {
    return <p>Carregando</p>
  }

  return (
    <div className={styles.albums}>
      {data.map(a => (
        <Link
          key={a.id}
          href={`/?albumId=${a.id}`}
          as={`/album/${a.id}`}
          scroll={false}
        >
          <a>
            <CardAlbum album={a} selectedCallback={props.selectCardCallback} />
          </a>
        </Link>
      ))}
    </div>
  )
}

export default CardList
