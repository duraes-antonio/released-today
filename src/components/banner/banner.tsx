import React from 'react'
import style from './banner.module.scss'

const Banner = (props: { date: Date }) => {
  return (
    <div className={style.banner}>
      <h1 className={style.title}>
        Released <b>Today!</b>
      </h1>
      <h3 className={style.description}>
        See the Heavy Metal music albums that were released on:
      </h3>
      <h2>
        {props.date.toLocaleDateString(undefined, {
          weekday: undefined,
          day: 'numeric',
          month: 'long',
          year: 'numeric'
        })}
      </h2>
    </div>
  )
}

export default Banner
