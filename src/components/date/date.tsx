import { DatePicker } from 'antd'
import moment from 'moment'
import React from 'react'
import styles from './date.module.scss'
import 'antd/dist/antd.css'

const DateBar = (props: {
  initial?: Date
  cb: (date: Date) => void
}): JSX.Element => {
  const value = props.initial && moment(props.initial)
  return (
    <div className={styles.bar}>
      <DatePicker
        size={'large'}
        defaultValue={value}
        onChange={selected => props.cb(selected?.toDate() ?? new Date())}
      />
    </div>
  )
}

export default DateBar
