import React from 'react'
import styles from './card.module.scss'
import { MAAlbum } from '../../services/metal-archives.service'

const CardAlbum = (props: {
  album: MAAlbum
  selectedCallback?: (a: MAAlbum) => void
}): JSX.Element => {
  return (
    <div
      className={styles.album}
      onClick={() =>
        props.selectedCallback && props.selectedCallback(props.album)
      }
    >
      {props.album.imageUrl && (
        <img
          alt={`${props.album.name} album cover art`}
          src={props.album.imageUrl}
          className={styles.image}
        />
      )}
      <table className={styles.info}>
        <tbody>
          <tr>
            <th className={styles.artist}>Artist</th>
            <td>{props.album.artist}</td>
          </tr>
          <tr>
            <th className={styles.title}>Title</th>
            <td>{props.album.name}</td>
          </tr>
          <tr>
            <th className={styles.title}>Format</th>
            <td>{props.album.format}</td>
          </tr>
        </tbody>
      </table>
    </div>
  )
}

export default CardAlbum
